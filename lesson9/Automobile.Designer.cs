﻿namespace WindowsFormsApplication1
{
    partial class Automobile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topspeedlabel = new System.Windows.Forms.Label();
            this.mpglabel = new System.Windows.Forms.Label();
            this.gastanklabel = new System.Windows.Forms.Label();
            this.doorslabel = new System.Windows.Forms.Label();
            this.sunrooflabel = new System.Windows.Forms.Label();
            this.colorlabel = new System.Windows.Forms.Label();
            this.topspeedbox = new System.Windows.Forms.TextBox();
            this.mpgbox = new System.Windows.Forms.TextBox();
            this.gastankbox = new System.Windows.Forms.TextBox();
            this.doorsbox = new System.Windows.Forms.TextBox();
            this.colorbox = new System.Windows.Forms.TextBox();
            this.sunroofbox = new System.Windows.Forms.CheckBox();
            this.autobutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // topspeedlabel
            // 
            this.topspeedlabel.AutoSize = true;
            this.topspeedlabel.Location = new System.Drawing.Point(40, 45);
            this.topspeedlabel.Name = "topspeedlabel";
            this.topspeedlabel.Size = new System.Drawing.Size(60, 13);
            this.topspeedlabel.TabIndex = 0;
            this.topspeedlabel.Text = "Top Speed";
            // 
            // mpglabel
            // 
            this.mpglabel.AutoSize = true;
            this.mpglabel.Location = new System.Drawing.Point(40, 93);
            this.mpglabel.Name = "mpglabel";
            this.mpglabel.Size = new System.Drawing.Size(83, 13);
            this.mpglabel.TabIndex = 1;
            this.mpglabel.Text = "Miles Per Gallon";
            // 
            // gastanklabel
            // 
            this.gastanklabel.AutoSize = true;
            this.gastanklabel.Location = new System.Drawing.Point(43, 144);
            this.gastanklabel.Name = "gastanklabel";
            this.gastanklabel.Size = new System.Drawing.Size(77, 13);
            this.gastanklabel.TabIndex = 2;
            this.gastanklabel.Text = "Gas Tank Size";
            // 
            // doorslabel
            // 
            this.doorslabel.AutoSize = true;
            this.doorslabel.Location = new System.Drawing.Point(40, 194);
            this.doorslabel.Name = "doorslabel";
            this.doorslabel.Size = new System.Drawing.Size(87, 13);
            this.doorslabel.TabIndex = 4;
            this.doorslabel.Text = "Number of Doors";
            // 
            // sunrooflabel
            // 
            this.sunrooflabel.AutoSize = true;
            this.sunrooflabel.Location = new System.Drawing.Point(43, 224);
            this.sunrooflabel.Name = "sunrooflabel";
            this.sunrooflabel.Size = new System.Drawing.Size(52, 13);
            this.sunrooflabel.TabIndex = 5;
            this.sunrooflabel.Text = "Sun Roof";
            this.sunrooflabel.Click += new System.EventHandler(this.label6_Click);
            // 
            // colorlabel
            // 
            this.colorlabel.AutoSize = true;
            this.colorlabel.Location = new System.Drawing.Point(43, 267);
            this.colorlabel.Name = "colorlabel";
            this.colorlabel.Size = new System.Drawing.Size(31, 13);
            this.colorlabel.TabIndex = 6;
            this.colorlabel.Text = "Color";
            // 
            // topspeedbox
            // 
            this.topspeedbox.Location = new System.Drawing.Point(192, 45);
            this.topspeedbox.Name = "topspeedbox";
            this.topspeedbox.Size = new System.Drawing.Size(363, 20);
            this.topspeedbox.TabIndex = 7;
            // 
            // mpgbox
            // 
            this.mpgbox.Location = new System.Drawing.Point(194, 86);
            this.mpgbox.Name = "mpgbox";
            this.mpgbox.Size = new System.Drawing.Size(358, 20);
            this.mpgbox.TabIndex = 8;
            // 
            // gastankbox
            // 
            this.gastankbox.Location = new System.Drawing.Point(192, 141);
            this.gastankbox.Name = "gastankbox";
            this.gastankbox.Size = new System.Drawing.Size(358, 20);
            this.gastankbox.TabIndex = 9;
            // 
            // doorsbox
            // 
            this.doorsbox.Location = new System.Drawing.Point(190, 196);
            this.doorsbox.Name = "doorsbox";
            this.doorsbox.Size = new System.Drawing.Size(362, 20);
            this.doorsbox.TabIndex = 11;
            // 
            // colorbox
            // 
            this.colorbox.Location = new System.Drawing.Point(197, 276);
            this.colorbox.Name = "colorbox";
            this.colorbox.Size = new System.Drawing.Size(354, 20);
            this.colorbox.TabIndex = 13;
            // 
            // sunroofbox
            // 
            this.sunroofbox.AutoSize = true;
            this.sunroofbox.Location = new System.Drawing.Point(203, 230);
            this.sunroofbox.Name = "sunroofbox";
            this.sunroofbox.Size = new System.Drawing.Size(71, 17);
            this.sunroofbox.TabIndex = 14;
            this.sunroofbox.Text = "Sun Roof";
            this.sunroofbox.UseVisualStyleBackColor = true;
            // 
            // autobutton
            // 
            this.autobutton.Location = new System.Drawing.Point(206, 330);
            this.autobutton.Name = "autobutton";
            this.autobutton.Size = new System.Drawing.Size(83, 41);
            this.autobutton.TabIndex = 15;
            this.autobutton.Text = "Submit";
            this.autobutton.UseVisualStyleBackColor = true;
            this.autobutton.Click += new System.EventHandler(this.autobutton_Click);
            // 
            // Automobile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 474);
            this.Controls.Add(this.autobutton);
            this.Controls.Add(this.sunroofbox);
            this.Controls.Add(this.colorbox);
            this.Controls.Add(this.doorsbox);
            this.Controls.Add(this.gastankbox);
            this.Controls.Add(this.mpgbox);
            this.Controls.Add(this.topspeedbox);
            this.Controls.Add(this.colorlabel);
            this.Controls.Add(this.sunrooflabel);
            this.Controls.Add(this.doorslabel);
            this.Controls.Add(this.gastanklabel);
            this.Controls.Add(this.mpglabel);
            this.Controls.Add(this.topspeedlabel);
            this.Name = "Automobile";
            this.Text = "Automobile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label topspeedlabel;
        private System.Windows.Forms.Label mpglabel;
        private System.Windows.Forms.Label gastanklabel;
        private System.Windows.Forms.Label doorslabel;
        private System.Windows.Forms.Label sunrooflabel;
        private System.Windows.Forms.Label colorlabel;
        private System.Windows.Forms.TextBox topspeedbox;
        private System.Windows.Forms.TextBox mpgbox;
        private System.Windows.Forms.TextBox gastankbox;
        private System.Windows.Forms.TextBox doorsbox;
        private System.Windows.Forms.TextBox colorbox;
        private System.Windows.Forms.CheckBox sunroofbox;
        private System.Windows.Forms.Button autobutton;
    }
}

