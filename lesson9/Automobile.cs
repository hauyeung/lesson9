﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Automobile : Form
    {
        public Automobile()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void autobutton_Click(object sender, EventArgs e)
        {
            int topspeed = Convert.ToInt32(topspeedbox.Text);
            double mpg = Convert.ToDouble(mpgbox.Text);
            double gastanksize = Convert.ToDouble(gastankbox.Text);
            int numdoors = Convert.ToInt32(doorsbox.Text);
            Boolean sunroof = sunroofbox.Checked;
            string color = colorbox.Text;

            MyAutomobile m = new MyAutomobile(topspeed, mpg, gastanksize, numdoors, sunroof, color);
        }
    }
}
